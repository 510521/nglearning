import { Component, OnInit } from '@angular/core';
import { ProductService } from "app/products/product.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
   
  // this.products=this.productService.productList;
  // this.products = [];
  //  this.productService.productList.forEach(item=>{
  //   this.products.push({...item});
  // });
  }
products:any[];
  title = 'app works! changed';
  name = 'Indresh ';
  phone = 9953093009;
  green = 'green';
  red = '#ff00ff';
  divid = "test";
  size = '12px';
  show = true;


  type = 'HELLO1';
  fruits = [];
  fruitQty = [];

  constructor(private productService: ProductService) {

    this.fruits = ["BANANA", "APPLE", "GRAPE", "ORANGE", "MANGO"];
    this.fruitQty = [
      {name:"BANANA",qty:10},
      {name:"APPLE",qty:20},
      {name:"GRAPE",qty:30},
      {name:"ORANGE",qty:40},
      {name:"MANGO",qty:50},
      ];

      this.phone=343434;
  }

  toogle(evnt) {
    console.log(evnt);
    if (evnt.altKey) {
      this.show = !this.show;
      this.type = 'HELLO';
    }
  }

  onfocus() {
    console.log('got the focus');
  }

  removeItem(indx) {
    this.fruitQty.splice(indx,1);
  
  }


}

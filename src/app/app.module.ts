import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppSidebarComponent } from "app/sidebar.component";
import { AppNavbarComponent } from "app/app.navbar.component";
import { AppJamboComponent } from "app/app.jambo.component";
import { ProductListComponent } from "app/products/product-list.component";
import { ProductComponent } from "app/products/product.component";
import { ProductService } from "app/products/product.service";
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductModelFormComponent } from './product-model-form/product-model-form.component';
import { RouterModule, Routes } from '@angular/router';
import { AboutusComponent } from "app/aboutus/aboutus.component";
// import {AppSidebarComponent} from './sidebar.component';

import 'rxjs/Rx';
import { AppProductDetailComponent } from "app/products/product-detail.component";
import { ProductBlockComponent } from "app/products/product-block.component";
import { ProductDetailsActivateGaurd } from "app/products/product-details.gaurd";
import { ProductDetailsDeActivateGaurd } from "app/products/product-details-degaurd";

const routes: Routes = [
  { path: '', redirectTo: "home", pathMatch: "full" },
  { path: 'home', component: AppJamboComponent },
  {
    path: 'products', component: ProductBlockComponent,
    children: [
      { path: '', component: ProductListComponent },
      { path: 'details/:id/:name', component: AppProductDetailComponent,
       canActivate: [ProductDetailsActivateGaurd],
      canDeactivate:[ProductDetailsDeActivateGaurd] }
    ]
  },
  { path: 'aboutus', component: AboutusComponent },
  { path: "**", redirectTo: "", pathMatch: "full" }
];


@NgModule({
  declarations: [
    AppComponent,
    AppSidebarComponent,
    AppNavbarComponent,
    AppJamboComponent,
    ProductListComponent,
    ProductComponent,
    ProductFormComponent,
    ProductModelFormComponent,
    AboutusComponent,
    AppProductDetailComponent,
    ProductBlockComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule,
    RouterModule.forRoot(routes)

  ],
  providers: [ProductService,ProductDetailsActivateGaurd,ProductDetailsDeActivateGaurd],
  bootstrap: [AppComponent]
})
export class AppModule { }

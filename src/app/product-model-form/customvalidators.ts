import { FormControl } from "@angular/forms";

export function zipcode(c: FormControl) {
    if(!c.value){
        return;
    }
    let val= c.value;
    return /^\d{6}$/.test(val)?null:{zipcode:true}
}

export function nospace(c:FormControl) {
    if(!c.value){return;}
    let val:string = c.value;
    return val.includes(" ")?{nospace:true}:null;
}
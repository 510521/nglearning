import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl } from "@angular/forms";
import { zipcode, nospace } from "app/product-model-form/customvalidators";
@Component({
  selector: 'app-product-model-form',
  templateUrl: './product-model-form.component.html'

})
export class ProductModelFormComponent implements OnInit {

state=3;
  product: FormGroup;
  flag = false;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    // this.product = new FormGroup({
    //   "name":  new FormControl('',[]),
    //   "price": new FormControl('10')
    // });
    this.product = this.fb.group({
      name: ['Test', [nospace]],
      price: ['Again', []],
      dimension: this.fb.group({
        width:[''],
        height:['']
      }),
      items: this.fb.array([
        this.fb.control(""),
        this.fb.control(""),
        this.fb.control(""),
        this.fb.control(""),
      ]),
      details: this.fb.array([])
    });

    console.log(this.product);
  }
    toogleDimension(){
      let ctrl:AbstractControl = this.product.get("items");
     if(ctrl.disabled){
       ctrl.enable(true);
     }else {
       ctrl.disable(true);
     }
    }
  addDetails() {
    (<FormArray>this.product.get("details"))
    .push(this.fb.group({
      item: ["",Validators.required],
      qty: [0],
      price: [0.0]
    }));
  }

  removeDetail(indx) {
    (<FormArray>this.product.get("details")).removeAt(indx);
  }

  saveForm(nazakat) {
  }

  addressRequired() {
    this.flag = !this.flag;

    if (this.flag) {
      let group = this.fb.group({
        "addrline1": ['', [Validators.required,nospace]]
      });

      this.product.addControl("address", group);
      this.product.addControl("pincode",
       new FormControl('', [Validators.required, Validators.minLength(6),zipcode]))
    }
  }

}

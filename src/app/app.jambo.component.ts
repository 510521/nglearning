
import { Component } from "@angular/core";

@Component({
    selector:'app-jambo',
    template:`<div class="container">
    <div class="jumbotron">
        <h1>{{title}}</h1>
        <p>An E-Cart application for your products</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Browse the products</a></p>
    </div>
</div>`
})
export class AppJamboComponent {
title = "E-cart changed";


}
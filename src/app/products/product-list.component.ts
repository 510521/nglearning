import { Component, OnInit, AfterViewInit, OnDestroy, NgZone } from "@angular/core";
import { ProductService } from "app/products/product.service";
import { Product } from "app/products/product.model";
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styles: [
        '.tag {background:green;color:white;padding:3px;margin:3px;}',
        '.table { width:100%;table-layout:fixed }'
    ]
})
export class ProductListComponent implements OnInit, AfterViewInit {

    //global properties
    products: any;
    selectedItem: string;
    show = true;
    overview: any;

    // prodser:ProductService;


    constructor(private zone: NgZone, private prod: ProductService) {
        // this.prodser = prod;
        console.log(prod.title);

    }

    ngOnInit(): void {
        // var p = new Product();
        // this.products= this.prod.productList;
        //this.products = this.prodser.productList;
        console.log('Ng On Init');
        var zonel = this.zone;
        this.prod.getProdcuts().subscribe(
            function (res) {
                this.zone.run(() => {
                    this.products = res.json();
                })
                console.log(this.products);
            }.bind(this),
            (error) => {
                console.log(error)
            },
            () => {
                console.log("completed")
            });
        //use fat arrow function ()=> to get data from json
        this.prod.getOverview().subscribe((res) => {
            this.overview = res.json();
        });


    }

    ngAfterViewInit(): void {
        var myobserv = new Observable(observer => {
            setTimeout(function () {
                observer.next(10);
            }, 1000);

            setTimeout(function () {
                observer.next(50);
            }, 5000);

            setTimeout(function () {
                observer.next(100);
            }, 10000);

        });

        setTimeout(() => {
            myobserv.subscribe(res => {
                console.log("first: " + res);
            });
        }, 0);

        setTimeout(() => {
            myobserv.subscribe(res => {
                console.log("second: " + res);
            });
        }, 5000);

        myobserv.subscribe(res => {
            console.log("second: " + res);
        });


    }

    toogle() {
        this.show = !this.show;
    }
    onChildClick(data) {
        this.selectedItem = JSON.stringify(data);
    }

    incrementPrice(product: any, index: number) {
        product.price = product.price * 2;
        // product = null;
        // this.products[index] = {
        //         "id":15,
        //         "name": "A green mouse assigned again",
        //         "price": 25.50,
        //         "dimensions": {
        //             "length": 3.1,
        //             "width": 1.0,
        //             "height": 1.0
        //         },
        //         "warehouseLocation": {
        //             "latitude": 54.4,
        //             "longitude": -32.7
        //         }
        //     };
    }
}
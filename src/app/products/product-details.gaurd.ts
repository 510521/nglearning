
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable()
export class ProductDetailsActivateGaurd implements CanActivate {

    canActivate(route: ActivatedRouteSnapshot): boolean {
        let i = +route.params.id;
        if (isNaN(i) || i < 2 || i > 4) {
            return false;
        }
        return true;
    }

}
import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'product-block',
    templateUrl: 'product-block.component.html'
})

export class ProductBlockComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}
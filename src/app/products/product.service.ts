import { Injectable } from "@angular/core";
import { Http } from "@angular/http";



@Injectable()
export class ProductService {

    public title = "Product title";
    api = "assets/";
    constructor(private http: Http) { }

    public getProdcuts() {
        return this.http.get(this.api + 'products.json');
    }

    public getOverview() {
        return this.http.get(this.api + 'overview.json');
    }

    public getProductById(id: number) {
        return this.http.get(this.api + 'products.json')
            .map(res => {
                let data = res.json();
                let product:any[] = data.filter(i => { return i.id === id });
                return product && product.length ? product[0] : {};
            }

            );
    }

}
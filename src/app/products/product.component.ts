import { Component, Input, EventEmitter, Output, OnInit, OnDestroy, OnChanges, SimpleChanges, DoCheck } from "@angular/core";

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styles: [
        '.tag {background:green;color:white;padding:3px;margin:3px;}',
        '.table { width:100%;table-layout:fixed }'
    ]
})
export class ProductComponent implements OnInit,OnChanges, OnDestroy {
   

    @Input()
    index: number;

    @Input()
    product: any;

    @Output()
    onItemClick = new EventEmitter<any>();

    ngOnInit(): void {
        console.log("I m initilized" + this.product.name);
    }

     ngOnChanges(changes: SimpleChanges): void {
       for(let prop in changes) {
          if(prop ==="product"){
              console.log("Product property changed");
          }

       }
    }

    ngOnDestroy(): void {
        console.log("I m destroyed" + this.product.name);
        this.product = null;
    }
    onClick() {
        //  this.onItemClick.emit(`Selected product is : ${this.product.name}`);
        this.onItemClick.emit(this.product);
    }

}
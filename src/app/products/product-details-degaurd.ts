
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { AppProductDetailComponent } from "app/products/product-detail.component";

@Injectable()
export class ProductDetailsDeActivateGaurd implements CanDeactivate<AppProductDetailComponent> {

    canDeactivate(component: AppProductDetailComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot): boolean {
       
        return confirm("Are you sure want to leave?");
      
    }




}
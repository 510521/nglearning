import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "app/products/product.service";

@Component({
    selector: 'app-product-detail',
    templateUrl: 'product-detail.component.html'
})

export class AppProductDetailComponent implements OnInit {

    product: any;
    constructor(
        private activatedRoute: ActivatedRoute,
        private service: ProductService
    ) { }


    ngOnInit() {
        this.activatedRoute.params.subscribe((data) => {
            console.log("from params obervable", data);
            console.log("second log");
            this.service.getProductById(+data.id).subscribe(prod => this.product = prod);
        });

        // snapshot based
        let params = this.activatedRoute.snapshot.params;

        console.log("using snapshot as object", params);
        console.log("converting as number from string", +params.id);


    }
}